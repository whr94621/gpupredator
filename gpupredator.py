# MIT License

# Copyright (c) 2018 the GPUPredator authors.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import os
import subprocess
import sys
import time
from argparse import REMAINDER

import gpustat


def parse_args():
    parser = argparse.ArgumentParser(description="A program to search for idle GPUs and run your GPU task.")

    parser.add_argument("-i", "--id", type=int, nargs="+", help="ID of GPU you want to use.", default=[])

    parser.add_argument("-m", "--minimum_memory_consumption", type=float,
                        help="When memory consumption of a GPU is below this value, "
                             "GPUPredator treat it as an idle GPU", default=100)

    parser.add_argument("-t", "--time_interval", type=int,
                        help="Every this time, GPUPredator would search for idle GPUs.", default=5)

    parser.add_argument("training_script", type=str,
                        help="The full path to the GPU training script, "
                             "followed by all the arguments for this script.")

    parser.add_argument('training_script_args', nargs=REMAINDER)

    return parser.parse_args()


def main(args):
    if len(args.id) == 0:
        stats = gpustat.GPUStatCollection.new_query()
        args.id = map(lambda gpu: int(gpu.entry['index']), stats)

    while True:
        idle_gpus = []
        stats = gpustat.GPUStatCollection.new_query()
        gpu_memory = max(map(lambda gpu: float(gpu.entry['memory.total']), stats))
        idle_threshold = args.minimum_memory_consumption / gpu_memory
        ids = map(lambda gpu: int(gpu.entry['index']), stats)
        ratios = map(lambda gpu: float(gpu.entry['memory.used']) / float(gpu.entry['memory.total']), stats)

        # 1. Get idle gpu
        for item in zip(ids, ratios):
            if item[1] <= idle_threshold:
                idle_gpus.append(item[0])

        utilized_gpus = list(set(idle_gpus) & set(args.id))

        if len(utilized_gpus) > 0:
            break
        else:
            time.sleep(args.time_interval)

    # 2. Specify GPU
    current_env = os.environ.copy()
    current_env["CUDA_VISIBLE_DEVICES"] = ','.join([str(i) for i in utilized_gpus])

    cmd = [sys.executable,
           "-u",
           args.training_script] + args.training_script_args

    process = subprocess.Popen(cmd, env=current_env)
    process.wait()


if __name__ == '__main__':
    args = parse_args()
    main(args)
