# GPUPredator: Endless GPUs for you

![](https://ws1.sinaimg.cn/large/811de2f6ly1fx5mhfi2tsj20b4085q35.jpg)

This is a program to automatically search for idle GPUs and run your
GPU task.

## Requirements

gpustat

## HowTo
If you want to run a gpu program on a GPU whose memory consumption is
less than 100MB, you can run following command:

```bash
python -m gpupredator -m 100 -t 5 <your-gpu-program> <arguments-for-program>
```
GPUPredator will search for any idle GPU every 5 secs.

If you want to speficy the candidate ids of GPU, use option ```-i```.

See details by running

```bash
python -m gpupredator --help
```